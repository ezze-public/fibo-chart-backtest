There is a need of an agent, ready to receive new requests for process. the agent gets the requests in a get method, containing the details of a fibo-process, and the related date range.
then it will start the process of demonstrating buy and sell positions. finally after making the result log, and resulting profit or loss, it will store it in the following path:
html/data/{an-md5-named-directory}/.json

also the log content will be stored in a file named .log just beside the .json file
the fibo-chart agent that made the request will have the {md5-named-directory} at the beginning of the process (when it made the initial request), so it will store the md5 in log column of database. in next load when it faces with the md5 key, tries to check if the result is ready to fetch or not. then it will start to download the result from FCB (fibo-chart-backtest) agent, and store it on database.
in other words, the process starts from the FC (fibo-chart) agent, and the FCB just responds to it's requests, and put them somewhen accessible for FC.
