<?php


/**
 * v-2.0 2022-11-21
 */

class bybit
{
	

	public static function balance( $credentials, $coin, $only_available=false ){
		
		$res = self::request($credentials, 'GET', '/v2/private/wallet/balance');
		
		if( is_array($res) and array_key_exists('result', $res) and array_key_exists($coin, $res['result']) ){
			return $res['result'][$coin][ $only_available ? 'available_balance' : 'equity' ];

		} else {
			return 'N/A';
		}
				
	}


	public static $side_translate = [
		'long' => 'Buy',
		'sell' => 'Sell',
	];


	public static function order( $credentials, $type, $dimention, $symbol, $price, $volume ){
		
		if(! $data = self::exchangeInfo_read($symbol) ){
			log_sys("Can't load exchangeInfo for {$symbol}");

		} else {
			
			$max_volume = $data['lot_size_filter']['max_trading_qty'];
			
			if( $volume > $max_volume ){
				log_sys(" $volume > $max_volume ");
				while( true ){
					$part_volume = ( ($volume <= $max_volume) ? $volume : ($max_volume / 2) );
					$res[] = self::order($credentials, $type, $dimention, $symbol, $price, $part_volume);
					$volume-= $part_volume;
					if(! $volume )
						return $res;
				}
			}
		}

		# $dimention : open-sell, close-sell, open-long, close-long 
		list($close, $side) = explode('-', strtolower($dimention));
		$close = ($close == 'close');

		$position_idx = [
			'open-long' => 1,
			'open-sell' => 2,
			'close-long' => 1,
			'close-sell' => 2,
		][ $dimention ];
		
		if( $close )
			$side = self::side_reverse($side);

		$type = strtolower($type);

		if(! $close ){
			if( $volume * $price < 1 ){
				log_sys("the volume $volume of $symbol is lower than the threshold");
				fibo_chart::disconnect();
			}
			$volume = self::set_volume($symbol, $volume);
		}

		$volume = wash_my_float($volume);

		$price = self::set_price($symbol, $price);

		if(! $volume ){
            log_sys("order for $volume $symbol is not possible");
			fibo_chart::disconnect();
		}

		$symbol = str_replace('_', '', $symbol);

        switch( $type ){
            
            case 'limit': 
				$order_type = 'Limit';
                $time_in_force = 'GoodTillCancel';
                break;

            case 'fok':
				$order_type = 'Limit';
                $time_in_force = 'FillOrKill';
                break;

            case 'market':
				$order_type = 'Market';
                $time_in_force = 'GoodTillCancel';
                break;

        }
		
		$option_s =  [
			'symbol' => $symbol, 
			'side' => self::$side_translate[$side], 
			'order_type' => $order_type, 
			'qty' => $volume, 
			'time_in_force' => $time_in_force,
			'reduce_only' => $close,
			'close_on_trigger' => false,
			'position_idx' => $position_idx,
		];
		if( $type != 'market' )
			$option_s['price'] = $price;

		$res_order = self::request($credentials, 'POST', '/private/linear/order/create', $option_s);
		$order_id = $res_order['result']['order_id'];

        switch( $type ){

            case 'limit': 
                return $order_id;

            case 'fok':
                $res_status = bybit::order_status($credentials, $symbol, $order_id);
		        return $res_status['order_status'] == 'Filled' ? $order_id : false;

            case 'market':
                return $order_id;

		}

	}


	public static function order_list( $credentials, $symbol, $dimention=null ){

		$res = self::request($credentials, 'GET', '/private/linear/order/list', [
			'symbol'       => str_replace('_', '', $symbol),
            'order_status' => 'New,PartiallyFilled',
        ]);

		
		if( !is_array($res) or !array_key_exists('result', $res) or !array_key_exists('data', $res['result']) )
			return [];

		$order_s = $res['result']['data'];

		if( in_array( $order_s, [ NULL, 'NULL' ] ) )
			return [];

		if( $dimention ){

			list($close, $side) = explode('-', strtolower($dimention));
			$reduce_only = ($close == 'close');

			if( $reduce_only )
				$side = self::side_reverse($side);

			$side = self::$side_translate[$side];
			
			foreach( $order_s as $i => $o )
				if( $o['reduce_only'] != $reduce_only or $o['side'] != $side )
					unset($order_s[$i]);
			
		}

		return $order_s;

	}



	public static function order_status( $credentials, $symbol, $order_id ){
		
        $res = self::request($credentials, 'GET', '/private/linear/order/search', [
            'symbol' => str_replace('_', '', $symbol),
            'order_id' => $order_id,
	    ]);

		return $res['result'];

	}



	public static function order_cancel( $credentials, $symbol, $order_id ){
		
        return self::request($credentials, 'POST', '/private/linear/order/cancel',[
			'symbol' => str_replace('_', '', $symbol),
			'order_id' => $order_id,
		]);

	}


	public static function close_all_orders( $credentials, $symbol ){

		$order_s = bybit::order_list($credentials, $symbol);

		if( sizeof($order_s) )
			foreach( $order_s as $i => $p )
				self::order_cancel($credentials, $symbol, $p['order_id']);
						
	}


	public static function close_all_positions( $credentials, $symbol ){

		$position_s = bybit::position_list($credentials, $symbol);

		if( sizeof($position_s) ){
			foreach( $position_s as $i => $p ){
				$dimention = 'close-'.array_flip(self::$side_translate)[$p['side']];
				self::order($credentials, 'MARKET', $dimention, $symbol, null, $p['size']);
			}
		}

	}


	public static function position_list( $credentials, $symbol ){
		
		$res = self::request($credentials, 'GET', '/private/linear/position/list', [
			'symbol'    => str_replace('_', '', $symbol)
		]);
		
		if( !is_array($res) or !array_key_exists('result', $res) )
			return [];

		$res = $res['result'];

		if( in_array( $res, [NULL, 'NULL'] ) ){
			$res = [];

		} else foreach( $res as $i => $pos ){
			if( $pos['size'] == 0 )
				unset($res[$i]);
		}
		
		return $res;

	}


	public static function price( $credentials, $symbol ){
		
		$res = self::request($credentials, 'GET', '/public/linear/recent-trading-records', [
            'symbol' => str_replace('_', '', $symbol)
        ]);
		
		if(! $price = $res['result'][0]['price'] ){
			sleep(1);
			return self::price($symbol);
		
		} else {
			return $price;
		}

	}


	public static function set_mode( $credentials, $symbol, $mode ){
		// MergedSingle, BothSide
		return self::request($credentials, 'POST', '/private/linear/position/switch-mode', [
			'symbol' => str_replace('_', '', $symbol),
			'mode' => $mode,
		]);

	}


	public static function set_leverage( $credentials, $symbol, $sell_leverage, $buy_leverage ){

		return self::request($credentials, 'POST', '/private/linear/position/set-leverage', [ 
			'symbol' => str_replace('_', '', $symbol),
			'sell_leverage' => $sell_leverage,
			'buy_leverage' => $buy_leverage,
		]);

	}


	public static function transfer( $credentials, $coin, $volume, $target ){

		$sha1 = sha1(uniqid().rand(1000,9999));
		$transfer_id = 
			'selfTransfer_'.
			substr($sha1, 0,8).'-'.
			substr($sha1, 8,4).'-'.
			substr($sha1,12,4).'-'.
			substr($sha1,16,4).'-'.
			substr($sha1,20,12);
		
		$volume = strval($volume);

		$target = strtoupper($target);
		$source = ($target == 'SPOT') ? 'CONTRACT' : 'SPOT';

		var_dump([ 
			'transfer_id' => $transfer_id,
			'coin' => $coin,
			'amount' => $volume,
			'from_account_type' => $source,
			'to_account_type' => $target,
		]);

		return self::request($credentials, 'JSON', '/asset/v1/private/transfer', [ 
			'transfer_id' => $transfer_id,
			'coin' => $coin,
			'amount' => $volume,
			'from_account_type' => $source,
			'to_account_type' => $target,
		]);

	}


	public static function spot_balance( $credentials, $coin, $only_available=false ){
		
		if( $res = self::request($credentials, 'GET', '/spot/v3/private/account') ){
            if( sizeof($res['result']['balances']) ){
                foreach( $res['result']['balances'] as $b ){
                    if( $b['coin'] == strtoupper($coin) ){
                        return $only_available 
                            ? $b['free'] 
                            : $b['total'];
                    }
                }
            }
        }
		
		return 0;

	}


	public static function set_price( $symbol, $p ){
		
		$symbol = str_replace('_', '', $symbol);
		$ps = self::exchangeInfo_symbols()[$symbol]['price_step'];
		$p = floor($p / $ps) * $ps;
		$p = strval($p);
		
		return $p;
		
	}


	public static function set_volume( $symbol, $v ){

		$symbol = str_replace('_', '', $symbol);
		$vs = self::exchangeInfo_symbols()[$symbol]['volume_step'];
		
		$v = strval($v / $vs);
		$v = floor($v) * $vs;
		$v = strval($v);

		return $v;

	}


	public static function exchangeInfo_symbols(){

		$data = self::exchangeInfo_read();
		
		foreach( $data as $item ){
			extract($item);

			if(! in_array($quote_currency, [ 'USDT', 'BUSD', 'USDC' ]) )
                continue;
                
            if( $status != 'Trading' )
                continue;
			
			$symbol_s[ $base_currency.$quote_currency ] = [
				'price_step' => $price_filter['min_price'],
				'volume_step' => $lot_size_filter['qty_step'],
				'max_leverage' => $leverage_filter['max_leverage'],
			];

		}

		ksort($symbol_s);
		return $symbol_s;

	}

	
	private static $exchangeInfo_path = '/var/www/.exchangeInfo';


	private static function exchangeInfo_read( $symbol = null ){
		
		self::exchangeInfo_sync();
		$data = file_get_contents(self::$exchangeInfo_path);
		$res = json_decode($data, true)['result'];

		if( $symbol = str_replace('_', '', strtoupper($symbol)) ){
			
			foreach( $res as $item )
				if( $item['name'] == $symbol )
					return $item;

			return false;

		} else {
			return $res;
		}

	}


	private static function exchangeInfo_sync(){
		
		$file = self::$exchangeInfo_path;

		if(! file_exists($file) or filemtime($file) < date('U') - 3600*24 ){

			$temp = sys::take_care_of_file($file.'.temp');

			if(! copy('https://api.bybit.com/v2/public/symbols', $temp) ){
				log_sys("exchangeInfo_sync:".__LINE__);
                
			} else if(! file_exists($temp) ){
				log_sys("exchangeInfo_sync:".__LINE__);
			
			} else if(! $data = file_get_contents($temp) ){
				log_sys("exchangeInfo_sync:".__LINE__);
			
			} else if(! $data = json_decode($data, true)['result'] ){
				log_sys("exchangeInfo_sync:".__LINE__);
			
			} else if(! sizeof($data) ){
				log_sys("exchangeInfo_sync:".__LINE__);
			
			} else {
				
				if( file_exists($file) )
					sys::force_unlink($file);

				rename($temp, $file);

			}
		
		}

	}


    private static function request( $credentials, $method, $path, $params=[], $control=true ){

		$GLOBALS['bybit']['errMsg'] = '';

        while( true ){
            
            if( $res = self::request_base($credentials, $method, $path, $params) ){
				
				if( $res[ array_key_exists('retMsg', $res)? 'retMsg' : 'ret_msg' ] === 'OK' )
            	    break;
				
				// position mode not modified
				if( $res['ret_code'] == 30083 )
					break;

				// leverage not modified
				if( $res['ret_code'] == 34036 )
					break;
				
				if( $res['ret_code'] == 33004 ){
					
					if( defined('RUN_BY_PANEL') ) {
						$GLOBALS['bybit']['errMsg'] = 'API KEY expired';
						return 'N/A';

					} else {
						log_sys('API KEY expired.');
						fibo_chart::disconnect();
					}

				}

			}

            if(! $control )
                return 'N/A';
			
            $dk = debug_backtrace();
            $log_text = 'bybit::request '.
                $dk[1]['function'].":".$dk[0]['line'].' '.
				// json_encode($dk)." ".
                // $credentials[0].' '.
                "$method $path ".json_encode($params).' '.
				" ==> res: ".json_encode($res).
				'';
			
            if( ++$k == 6 ){

				if( defined('RUN_BY_PANEL') ){
					return 'N/A';

				} else {
					log_sys("try end ".$log_text);
					die;
				}

            }

			if( defined('RUN_BY_PANEL') ){
				return 'N/A';

			} else {
				log_sys("try $k ".$log_text);
			}

        }

        return $res;

    }


	private static function request_base( $credentials, $method, $path, $params = [] ){

		$curl_uri = "https://api.bybit.com".$path;
		$params_signed = self::sign($credentials, $params);

		switch( strtoupper($method) ){
			
			case 'JSON':
				$curl = curl_init($curl_uri);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
				curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params_signed));
				break;
			
			case 'POST':
				$curl = curl_init($curl_uri."?".http_build_query($params_signed));
				curl_setopt($curl, CURLOPT_POST, true);
				break;
			
			default: 
				$curl = curl_init($curl_uri."?".http_build_query($params_signed));
				break;
			
		}

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

		$server_output = curl_exec($curl);
		return json_decode($server_output, true);

	}


	private static function sign( $credentials, $params ) {

		list($the_key, $the_secret) = explode(':', $credentials);
		$params = array_merge(['api_key' => $the_key, 'timestamp' => time() * 1000 ], $params);
		ksort($params);
		$signature = hash_hmac('sha256', urldecode(http_build_query($params)), $the_secret);
		$params['sign'] = $signature;
		
		return $params;

	}

	
    private static function side_reverse( $side ){
        return in_array($side, ['long', 'Buy']) ? 'sell' : 'long';
    }


}












