<?php


class backtest
{
    


    private static $balance = 10000;
    private static $init_balance;

    private static $open;
    private static $low;
    private static $high;
    private static $close;

    private static $min;
    private static $max;

    private static $now;

    private static $step_s;
    private static $base_volume;
    private static $curr_step;
    private static $curr_side;
    private static $pos;
    private static $trigger_s;

    private static $round = 0;
    private static $profit = 0;
    private static $percent = 0;
    private static $risk;
    private static $log;

    private static $round_filled;
    private static $step_filled;

    private static $i;

    private static $liquidated;
    private static $date_start;

    private static $liq_price_cache;


    public static function handle(){

        self::param_control();
        self::$init_balance = self::$balance;
        self::sync_price();

        while(true){ // counts the rounds.

            self::$round_filled = false;
            self::sync_step(self::$balance , self::$close);

            self::log_db("- - - - -\n".date('Y-m-d H:i', self::$now)."  0: ROUND ".self::$round." @".self::$close);

            self::$date_start = self::$now;

            while(true){ // counts the steps.

                self::$step_filled = false;
                self::$liq_price_cache = null;
                self::sync_trigger();

                while(true){ // counts the prices.

                    self::sync_price();
                    self::liquidation();
                    self::do_the_trade();

                    if( self::$step_filled or self::$round_filled )
                        break;

                }

                if( self::$round_filled )
                    break;
            
            }

        }

    }


    private static function param_control(){

        global $argv;

        if(! $argv[1] )
            die('no PATH defined.');

        define('PATH', $argv[1]);
        define('PATH_FULL', '/var/www/html/data/'.PATH.'/');
        $_POST = json_decode(file_get_contents(PATH_FULL.'.post'), true);

        foreach( [ 'exchange', 'symbol', 'leverage', 'budget', 'step_gap', 'max_step', 'valid_step', 'date_from', 'date_to' ] as $p ){
            
            if(! $_POST[$p] ){
                die('the following parameter is nod defined: <b>'.$p.'</b>');
            
            } else {
                define( strtoupper($p), $_POST[$p] );
            }

        }
        
        define('BASE', explode('_', SYMBOL)[1]);
        
        return true;

    }


    private static function load_candle_s( $date_from, $date_to, $tf ){

        self::candle_available_semaphore($tf);

        $candle_url = CMH_NODE."/serve/binance/futures/".
            SYMBOL."/".$tf."/".self::date_formulate($date_from)."/".self::date_formulate($date_to);

        if(! $data = net::wjson($candle_url) ){
            if( $er_msg = trim( proc::error() ) )
                $er_msg = " [ error: {$er_msg} ]";
            die("candles not loading at {$candle_url}{$er_msg}\n");

        } else {
            array_shift($data);
            return $data;
        }
        
    }


    private static function candle_available_semaphore($tf){
        
        $url_avlb = CMH_NODE."/available/binance/futures/".SYMBOL."/".$tf;
        $url_need = CMH_NODE.'/need/binance/futures/'.SYMBOL."/".$tf;

        if( net::wjson($url_avlb) )
            return true;
        
        if(! net::wjson($url_need) ){
            self::log_db("CAN'T ASK FOR ".SYMBOL."/{$tf} ON CMH THROUGH {$url_need}");
            self::disconnect();
            
        } else {
            $msg = 'awaiting for pair '.SYMBOL;
            self::log_db($msg);
            code::json_die(['res'=>'ER', 'msg'=>$msg]);
        }

    }


    private static function date_formulate( $date ){
        $date = str_replace(' ', 'T', $date);
        $date = substr($date, 0, 16);
        return $date;
    }
    

    private static function charge_candle_stack(){

        $a_week = 604800;
        
        if(! array_key_exists('candle_stack', $GLOBALS) or !sizeof($GLOBALS['candle_stack']) ){
            
            if(! array_key_exists('tmp_DATE_FROM', $GLOBALS) ){
                $GLOBALS['tmp_DATE_FROM'] = DATE_FROM;
            } else {
                $GLOBALS['tmp_DATE_FROM'] = date('Y-m-d H:i:s', strtotime($GLOBALS['tmp_DATE_FROM']) + $a_week );
            }

            if( $GLOBALS['tmp_DATE_FROM'] >= DATE_TO )
                return false;

            $GLOBALS['tmp_DATE_TO'] = min( DATE_TO, date('Y-m-d H:i:s', strtotime($GLOBALS['tmp_DATE_FROM']) + $a_week -60 ) );
            $GLOBALS['candle_stack'] = self::load_candle_s($GLOBALS['tmp_DATE_FROM'], $GLOBALS['tmp_DATE_TO'], '1m');

        }

        return true;
        
    }


    private static function sync_price(){
        
        if(! self::charge_candle_stack() ){
            
            self::log_db("- - - - - -");
            self::disconnect();
        
        } else {

            $now = array_shift($GLOBALS['candle_stack']);

            self::$open = $now[1];
            self::$low = $now[2];
            self::$high = $now[3];
            self::$close = $now[4];

            self::$min = min($now[1], $now[2], $now[3], $now[4]);
            self::$max = max($now[1], $now[2], $now[3], $now[4]);

            if(! is_numeric($now[0]) ){
                echo $now[0]." is not numeric\n";
                die;
            }

            self::$now = $now[0]/1000;
            self::$i['price_checked']++;

        }

    }


    private static function sync_step( $balance, $price ){
        
        self::$curr_step = 0;
        self::$pos = [];

        self::set_base_volume($balance, $price);

        $price_up = $price;
        $price_dn = $price;

        foreach( math::fibo_serial(MAX_STEP) as $step => $fibo ){
            
            $price_up = $price_up * (1 + ( $fibo * STEP_GAP / 100 ));
            $price_dn = $price_dn * (1 - ( $fibo * STEP_GAP / 100 ));

            self::$step_s[     $step ] = [ 'price' => bybit::set_price(SYMBOL, $price_up), 'volume' => bybit::set_volume(SYMBOL, self::$base_volume * $fibo) ];
            self::$step_s[ -1* $step ] = [ 'price' => bybit::set_price(SYMBOL, $price_dn), 'volume' => bybit::set_volume(SYMBOL, self::$base_volume * $fibo) ];

        }

        krsort(self::$step_s);

    }


    private static function set_base_volume( $balance, $price ){

        $wallet_x = $balance * LEVERAGE * BUDGET; // available volume
        $wallet_x = floor($wallet_x); // round the USDT
        $volume = $wallet_x / $price; // make a division
        $portion = array_sum( math::fibo_serial(MAX_STEP) );
        $volume_portion = floor($wallet_x / $portion);
        $volume/= $portion; // grab the budget for step one, between the whole budget.
        $base_volume = bybit::set_volume(SYMBOL, $volume); // cut the edge

        if(! $base_volume ){
            $volume_step_base = ceil(bybit::exchangeInfo_symbols()[SYMBOL]['volume_step'] * $price);
            self::log_db("BASE_VOLUME of $volume_portion ".BASE." is less than the threshold of $volume_step_base ".BASE);
            self::disconnect();
        }

        self::$base_volume = $base_volume;
        
    }


    private static function sync_trigger(){

        $step_up = self::$curr_step +1;
        $step_dn = self::$curr_step -1;

        self::$trigger_s = [];

        if( abs($step_up) <= VALID_STEP and array_key_exists($step_up, self::$step_s) ){
            self::$trigger_s['over']['step'] = $step_up;
            self::$trigger_s['over']['close'] = intval(self::$curr_step < 0);
        }
        
        if( abs($step_dn) <= VALID_STEP and array_key_exists($step_dn, self::$step_s) ){
            self::$trigger_s['undr']['step'] = $step_dn;
            self::$trigger_s['undr']['close'] = intval(self::$curr_step > 0);
        }

    }


    private static function do_the_trade(){
        
        if(! $GLOBALS['do_the_trade']['step_side'] ){

            $step_over = array_key_exists('over', self::$trigger_s) ? self::$step_s[ self::$trigger_s['over']['step'] ] : null;
            $step_undr = array_key_exists('undr', self::$trigger_s) ? self::$step_s[ self::$trigger_s['undr']['step'] ] : null;

            $GLOBALS['do_the_trade']['step_over'] = $step_over;
            $GLOBALS['do_the_trade']['step_undr'] = $step_undr;
        
        } else {
            $step_over = $GLOBALS['do_the_trade']['step_over'];
            $step_undr = $GLOBALS['do_the_trade']['step_undr'];
        }

        if( $step_over and self::$max >= $step_over['price'] ){
            
            $GLOBALS['do_the_trade']['step_side'] = null;

            $sec = time::sec_to_hour_n_min(self::$now - self::$date_start);

            if( self::$trigger_s['over']['close'] ){ # \/
                
                // close long
                self::$round_filled = true;
                self::$round++;

                $profit = self::$pos['long']['volume'] * $step_over['price'] - self::$pos['long']['invest'];
                $profit = round($profit, 2);
                self::$profit+= $profit;
                $percent = round($profit *100 / self::$init_balance, 2);
                self::$percent+= $percent;
                self::$balance+= $profit;
                self::$curr_step++;

                self::log_db("CLOSE SELL @".$step_over['price']." AFTER $sec, {$percent}%, TOTAL ".self::$percent."%", true);
                
                self::$curr_step = 0;

            } else { # /
                
                // sell
                self::$pos['sell']['volume'] += $step_over['volume'];
                self::$pos['sell']['invest'] += $step_over['volume'] * $step_over['price'];   
                
                self::$step_filled = true;
                self::$curr_step++;
                self::log_db("OPEN SELL @".$step_over['price']." v".$step_over['volume']." AFTER $sec", true);

                self::$curr_side = 'sell';

            }

            self::$date_start = self::$now;

        } else if( $step_undr and self::$min <= $step_undr['price'] ){

            $GLOBALS['do_the_trade']['step_side'] = null;
            $sec = time::sec_to_hour_n_min(self::$now - self::$date_start);

            if( self::$trigger_s['undr']['close'] ){ # /\
                
                // close sell
                self::$round_filled = true;
                self::$round++;

                $profit = self::$pos['sell']['invest'] - self::$pos['sell']['volume'] * $step_undr['price'];
                $profit = round($profit, 2);

                self::$profit+= $profit;
                $percent = round($profit *100 / self::$init_balance, 2);
                self::$percent+= $percent;
                self::$balance+= $profit;
                self::$curr_step--;

                self::log_db("CLOSE LONG @".$step_undr['price']." AFTER $sec, {$percent}%, TOTAL ".self::$percent."%", true);
                
                self::$curr_step = 0;

            } else { # \

                // long
                self::$pos['long']['volume'] += $step_undr['volume'];
                self::$pos['long']['invest'] += $step_undr['volume'] * $step_undr['price'];

                self::$step_filled = true;
                self::$curr_step--;
                self::log_db("OPEN LONG @".$step_undr['price']." v".$step_undr['volume']." AFTER $sec", true);

                self::$curr_side = 'long';

            }

            self::$date_start = self::$now;

        }
        
    }


    private static $LIQ_RATE = 0.006180666667;

    private static function liquidation(){

        if( $side = self::$curr_side ){

            if( array_key_exists($side, self::$pos) ){

                if(! self::$liq_price_cache ){

                    $entry_price = self::$pos[$side]['invest'] / self::$pos[$side]['volume'];
                    $liquid_perc = self::$balance / self::$pos[$side]['invest'];

                    self::$liq_price_cache['long']['liquid_price'] = $entry_price * ( 1 - $liquid_perc ) + ( self::$LIQ_RATE * $entry_price );
                    self::$liq_price_cache['long']['liq_price'] = bybit::set_price(SYMBOL, self::$liq_price_cache['long']['liquid_price']);

                    self::$liq_price_cache['sell']['liquid_price'] = $entry_price * ( 1 + $liquid_perc ) - ( self::$LIQ_RATE * $entry_price );
                    self::$liq_price_cache['sell']['liq_price'] = bybit::set_price(SYMBOL, self::$liq_price_cache['sell']['liquid_price']);

                }
                
                switch( $side ){

                    case 'long': 
                        if( self::$min <= self::$liq_price_cache[$side]['liquid_price'] ){
                            self::$liquidated = [
                                'price' => self::$liq_price_cache[$side]['liq_price'],
                                'date' => date('Y-m-d H:i', self::$now),
                            ];
                            self::disconnect();
                        } else {
                            $risk = round( abs(self::$min - self::$liq_price_cache[$side]['liquid_price']) *100 / self::$min, 2) - 1;
                            self::$risk = self::$risk ? min(self::$risk, $risk) : $risk;
                        }
                        break;

                    case 'sell':
                        if( self::$max >= self::$liq_price_cache[$side]['liquid_price'] ){
                            self::$liquidated = [
                                'price' => self::$liq_price_cache[$side]['liq_price'],
                                'date' => date('Y-m-d H:i', self::$now),
                            ];
                            self::disconnect();
                        } else {
                            $risk = round( abs(self::$max - self::$liq_price_cache[$side]['liquid_price']) *100 / self::$max, 2) - 1;
                            self::$risk = self::$risk ? min(self::$risk, $risk) : $risk;
                        }
                        break;

                }
                
            }

        }
        
    }

    
    private static function disconnect(){
        
        $count = number_format(self::$i['price_checked']);
        $sec = time::sec_to_hour_n_min(self::$now - strtotime(DATE_FROM));

        self::log_db("\n * SYMBOL ".SYMBOL."\n * BUDGET ".BUDGET."\n * LEVERAGE ".LEVERAGE."\n * STEP_GAP ".STEP_GAP."\n * MAX_STEP ".MAX_STEP."\n * VALID_STEP ".VALID_STEP."\n");
        self::log_db(" * ".self::$percent."% AT ROUND ".self::$round);

        if( self::$liquidated ){
            
            $liq_price = self::$liquidated['price'];
            $liq_date = self::$liquidated['date'];

            $sec0 = time::sec_to_hour_n_min(self::$now - self::$date_start);            
            self::log_db(" * LIQUIDATED ".strtoupper(self::$curr_side)." @{$liq_price} AFTER $sec0 ON {$liq_date} [ $count prices in $sec ]\n");

        } else {
            self::log_db(" * FINN [ $count prices in $sec ".( self::$risk ? ", ".self::$risk."% away from liq price" : "no liq price" )." ]\n\n- - - - - -");
        }

        self::save_result();
        die;

    }


    private static function save_result(){
        
        sys::take_care_of_dir(PATH_FULL);
        $json = json_encode([ 
            'status' => 'OK', 
            'res' => [
                'risk' => ( self::$risk ? self::$risk.'%' : "no risk" ),
                'round' => self::$round, 
                'percent' => number_format(self::$percent,2),
                'liq_price' => self::$liquidated['price'], 
                'liq_date' => self::$liquidated['date'], 
            ]
        ], JSON_PRETTY_PRINT);

        file_put_contents(PATH_FULL.'.json', $json);
        file_put_contents(PATH_FULL.'.log', self::$log);

    }



    private static function log_db( $text, $step=false ){
        
        if( $step )
            $text = date('Y-m-d H:i', self::$now)." ".self::curr_step_sign(). self::$curr_step.": ".$text;

        $text.= "\n";

        echo $text;
        self::$log.= $text;

    }
    
    
    private static function curr_step_sign(){
        return self::$curr_step == 0 
            ? ' '
            : (
                self::$curr_step > 0
                    ? '+'
                    : ''
            );
    }


}


