<?php


# etc class
# 2023-06-25


class time {
    

    public static function sec_to_hour_n_min( $time ){
        
        $time = round($time/60);
        
        if( $time < 60 ){
            $hour_n_min = "00:".self::addzero($time);
    
        } else {
            
            $m = $time%60;
            $h = floor($time/60);
            
            if( $h < 24 ){
                $hour_n_min = self::addzero($h).":".self::addzero($m);
    
            } else {
                $d = floor($h/24);
                $h = $h%24;
                $hour_n_min = "{$d}d.".self::addzero($h).":".self::addzero($m);
            }
        }
    
        return $hour_n_min;
    
    }
    
    
    private static function addzero($n){
        return ( strlen($n) == 1 ) ? "0{$n}" : $n;
    }


    public static function date_in_iran( $format='Y-m-d H:i:s' ){

        $date = gmdate('U') + 3.5 * 3600;
        $date = date( $format, $date);
        
        return $date;

    }


    public static function date_diff_now( $date ){
        return self::date_in_iran('U') - strtotime($date);
    }


    public static function date_atomize( $date ){
        
        $date = substr($date, 0, 16);
        $date = str_replace(' ', 'T', $date);
        
        return $date;

    }


    public static function date_unatomize( $date ){

        $date = str_replace('T', ' ', $date);
        $date.= ':00';

        return $date;

    }
    

}


class code {

    public static function json_die( $json ){
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($json);
        die;
    }

    public static function is_json( $text ){
        json_decode($text);
        $res = json_last_error() === JSON_ERROR_NONE;
        return $res;
    }

    public static function array_from_json( $json ){
    
        if( $json ){
            if( $json != 'null' ){
                $json = json_decode($json, true);
                if( sizeof($json) ){
                    return $json;
                }
            }
        }
    
        return [];
        
    }

    public static function start_with( $text, $start ){
        return ( substr($text, 0, strlen($start) ) == $start );
    }

}


class sys {

    public static function take_care_of_file( $file ){

        self::take_care_of_dir(dirname($file));
        
        if(! file_exists($file) )
            shell_exec(" sudo touch $file && sudo chown www-data:www-data $file ");

        if(! file_exists($file) )
            die("Can't create file: $file");

        return $file;

    }


    public static function take_care_of_dir( $dir ){

        if(! file_exists($dir) )
            shell_exec(" sudo mkdir -p $dir && sudo chown -R www-data:www-data $dir ");

        if(! file_exists($dir) )
            die("Can't create dir: $dir");

        return $dir;

    }


    public static function force_unlink( $file ){
        
        @unlink($file);

        if( file_exists($file) ){
            
            shell_exec(" sudo rm -rf $file ");
            
            if( file_exists($file) )
                die("Can't unlink file: $file");
            
        }

        return true;

    }

}


class proc {

    private static $error_msg;

    public static function error( $value=null ){
        
        # set value
        if(! is_null($value) ){
            self::$error_msg = $value;
        
        # get value
        } else {
            $msg = self::$error_msg;
            self::$error_msg = '';
        }
        
        return $msg;

    }

}


class net {

    public static function wget( $url, $post_params=[], $etc=[] ){

        if( array_key_exists('timeout', $etc) )
            $http_arr['timeout'] = intval($etc['timeout']);

        $user_agent = array_key_exists('user_agent', $etc) 
            ? $etc['user_agent'] 
            : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36';
            
        $http_arr['header'] = "User-Agent: {$user_agent}\r\n";

        if( sizeof($post_params) ){
            $http_arr['header'].= 'Content-Type: application/x-www-form-urlencoded';
            $http_arr['method'] = 'POST';
            $http_arr['content'] = http_build_query($post_params);
        }

        $contx = stream_context_create([
            'http' => $http_arr,
            'ssl' => [ 'verify_peer'=>false, 'verify_peer_name'=>false ],
        ]);
        
        return @file_get_contents($url, false, $contx);
        
    }


    public static function wjson( $url, $post_params=[], $etc=[], $die=false ){
        
        if(! $res = self::wget( $url, $post_params, $etc ) ){
            $msg = "no content received from {$url} ".json_encode($post_params);

        } else if(! code::is_json($res) ){
            $msg = "not a json";

        } else if(! $res = json_decode(trim($res), true) ){
            $msg = "can't decode the json";
            
        } else if( $res['status'] != 'OK' ){
            $msg = $res['msg'];
            
        } else {
            return $res['res'];
        }

        if( $die ){
            die("{$msg}\n");

        } else {
            proc::error($msg);
            return false;
        }

    }

}


class math {


    public static function fibo_serial( $max ){
        
        $f[0] = 0;
        $f[1] = 1;
    
        for( $step=2; $step<=$max; $step++ ){
            $f[$step] = intval($f[$step-2]) + intval($f[$step-1]);
            $last = $f[ $step ];
        }
    
        return $f;
    
    }
    
    
}


