<?php


// $_POST = [
//     'exchange' => 'bybit',
//     'symbol' => 'BTCUSDT',
//     'leverage' => '10',
//     'budget' => '0.15',
//     'step_gap' => '0.9',
//     'max_step' => '8',
//     'valid_step' => '6',
//     'date_from' => '2022-01-10T20:00',
//     'date_to' => '2022-06-12T20:00'
// ];


include_once('../inc/.php');

// code::json_die([ 'status'=>'OK', 'res'=>['msg'=>'processing', 'path'=>'0000/0000/'.date('U')] ]);

# checking whole of the incomming params
foreach( ['exchange', 'symbol', 'leverage', 'budget', 'step_gap', 'max_step', 'valid_step', 'date_from', 'date_to'] as $param )
    if(! $_POST[$param] )
        code::json_die(['status'=>'ER', 'msg'=>'the following parameter is not defined: '.$param]);
    else if( code::start_with($param, 'date_') and !strstr($_POST[$param], 'T') )
        code::json_die(['status'=>'ER', 'msg'=>"wrong ${param} pattern: ".$_POST[$param]]);

# making the key
ksort($_POST);
$md5 = md5(json_encode($_POST));

# generating the path
$PATH = substr($md5,0,4) .'/'. substr($md5,4,4) .'/'.substr($md5,8);
$PATH_FULL = "/var/www/html/data/${PATH}/";

# if the request is already in list, and maybe completed
if( file_exists($PATH_FULL.'.post') ){
    code::json_die([ 'status'=>'OK', 'res'=>['msg'=>'processed', 'path'=>$PATH] ]);

} else {

    # put the request on the desk
    sys::take_care_of_dir($PATH_FULL);
    file_put_contents($PATH_FULL.'.post', json_encode($_POST, JSON_PRETTY_PRINT));

    # ask the system to proceed it
    echo shell_exec(" /usr/bin/php /var/www/handle.php '${PATH}' >/dev/null 2>&1 & ");

    # return the receipt to the client
    code::json_die([ 'status'=>'OK', 'res'=>['msg'=>'processing', 'path'=>$PATH] ]);

}


// ** if the .post is due dated, remove it, and kill the related process.



