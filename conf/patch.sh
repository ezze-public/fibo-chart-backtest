
cd /var
git clone https://gitlab.com/ezze-public/fibo-chart-backtest.git
rm -rf www
mv fibo-chart-backtest www
sed -i 's,https://gitlab.com/,git@gitlab.com:,g' /var/www/.git/config

cp /var/www/conf/.env-sample /var/www/.env

echo 'www-data ALL=NOPASSWD: ALL' >> /etc/sudoers
chown -R www-data:www-data /var/www

cd /var/www
git config --global --add safe.directory /var/www
git config pull.rebase false
